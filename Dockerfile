FROM openjdk:11.0.8-slim-buster
EXPOSE 8888

ADD target/us-config.jar us-config.jar
ENTRYPOINT ["java", "-jar", "us-config.jar"]
