package com.country.us.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class UsConfigApplication {

	public static void main(String[] args) {
		System.out.println("Starting the config application...");
		SpringApplication.run(UsConfigApplication.class, args);
	}
}
